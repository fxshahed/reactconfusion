import React, { Component } from "react";
import { Card, CardImg, CardText, CardBody, CardTitle } from "reactstrap";

class DishDetail extends Component {
  componentDidMount() {
    console.log("DishDetail Component componentDidMount invoked");
  }

  componentDidUpdate() {
    console.log("DishDetail Component componentDidUpdate invoked");
  }

  renderDish(dish) {
    return (
      <Card>
        <CardImg top src={dish.image} alt={dish.name} />
        <CardBody>
          <CardTitle>{dish.name}</CardTitle>
          <CardText>{dish.description}</CardText>
        </CardBody>
      </Card>
    );
  }

  renderComments(comments) {
    return comments.map((comment) => {
      const date = new Date(comment.date);
      console.log(date);
      return (
        <div>
          <p> {comment.comment} </p>
          <p>
            -- {comment.author},{" "}
            {new Intl.DateTimeFormat("en-US", {
              year: "numeric",
              month: "short",
              day: "2-digit",
            }).format(new Date(Date.parse(comment.date)))}
          </p>
        </div>
      );
    });
  }

  render() {
    const dish = this.props.dish;
    console.log("DishDetail Component render invoked");
    if (dish == null) {
      return <div></div>;
    } else {
      return (
        <div className="row">
          <div className="col-12 col-md-5 m-1">{this.renderDish(dish)}</div>
          <div className="col-12 col-md-5 m-1">
            <h4> Comments </h4>
            {this.renderComments(dish.comments)}
          </div>
        </div>
      );
    }
  }
}

export default DishDetail;
